//
//  testViewController.h
//  Adjust
//
//  Created by chao ma on 2019/8/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SendMailViewController : UIViewController
- (void)sendEmail:(NSString *)recipients title:(NSString *)subject productName:(NSString *)disPlayName;
@end

NS_ASSUME_NONNULL_END
