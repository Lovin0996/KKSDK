//
//  testViewController.m
//  Adjust
//
//  Created by chao ma on 2019/8/21.
//

#import "SendMailViewController.h"
#import <MessageUI/MessageUI.h>
#import "AdsConstant.h"

@interface SendMailViewController ()<MFMailComposeViewControllerDelegate>
{
    NSString *_recipients;
    NSString *_subject;
    NSString *_disPlayName;
}
@end

@implementation SendMailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
}

- (void)sendEmail:(NSString *)recipients title:(NSString *)subject productName:(NSString *)disPlayName {
    _recipients = recipients;
    _subject = subject;
    _disPlayName = disPlayName;
    [self presenToMailController];
}

- (void)presenToMailController {
    NSString *appVersion = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    NSString *deviceInfo = [NSString stringWithFormat:FEEDBACK_DEVICE_INFO_FORMAT, _disPlayName, appVersion, [UIDevice currentDevice].model,[UIDevice currentDevice].systemVersion];
    MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc]init];
    NSString* title = [@"iOS " stringByAppendingString:NSLocalizedString(@"App.Name" , nil)];
    [mailComposeViewController setSubject:title];
    [mailComposeViewController setToRecipients:@[@"feedback@picfuntech.com"]];
    [mailComposeViewController setMessageBody:deviceInfo isHTML:YES];
    mailComposeViewController.mailComposeDelegate = self;
    mailComposeViewController.modalPresentationStyle = UIModalPresentationPageSheet;
    mailComposeViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:mailComposeViewController animated:YES completion:^{}];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootViewController dismissViewControllerAnimated:YES completion:nil];
}


@end

