//
//  CommonUtils.h
//  KKSDK
//
//  Created by jay Win on 2019/6/28.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MessageUI/MessageUI.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import <AdsConstant.h>
#import <ARKit/ARKit.h>
#import "SendMailViewController.h"



@protocol CommonUtilsListener <NSObject>
-(void)onRequestPermissionStateResponse:(NSString *)adsState;
@end

@interface CommonUtils : NSObject
@property (nonatomic, weak) id<CommonUtilsListener>delegate;
+(CommonUtils *)sharedManager;
+(void)vibrator:(int)type;
+(void)sendEmail:(NSString *)recipients title:(NSString *)subject productName:(NSString *)disPlayName;
+(void)showCommentDialog;
+(void)copyStringToClipboard:(NSString *)content;
-(BOOL)hasInstallApp:(NSString *)urlSchemes;
-(void)shareNative:(NSString *)filePath;
-(void)saveToAlbum:(NSString *)filePath;
-(void)shareToInstagram:(NSString *)filePath;
+(BOOL)checkCameraPermission;
+(BOOL)checkAlbumPermission;
-(void)requestCameraPermission;
-(void)requestAlbumPermission;
+(BOOL)isIphoneXAbove;
+(NSString *)getLanguage;
+(NSString *)getLocale;
+(BOOL)isIPad;
+(BOOL)isSupportARKit;
+(void)checkUpdateAppVersion:(NSString *)appStoreID title:(NSString *)titleText positive:(NSString *)positiveButtonText negative:(NSString *)negativeButtonText message:(NSString *)customMessage;
@end

