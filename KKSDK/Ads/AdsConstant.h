//
//  AdsConstant.h
//  KKSDK
//
//  Created by Win Jay on 2019/3/8.
//  Copyright © 2019年 jay Win. All rights reserved.
//

#ifndef AdsConstant_h
#define AdsConstant_h


#define RECEIPT_VALIDATION_URL @"https://subscribe.buttondc.info/subscribe/post"




#pragma mark   Unity LayaCallback
#define INIT_SUCCEED @"INIT_SUCCEED"
#define BANNER_LOADED @"BANNER_LOADED"
#define BANNER_SHOW @"BANNER_SHOW"
#define BANNER_FAILED @"BANNER_FAILED"
#define BANNER_CLICK @"BANNER_CLICK"
#define BANNER_HIDE @"BANNER_HIDE"
#define BANNER_REQUEST @"BANNER_REQUEST"
#define BANNER_IM @"BANNER_IMPRESSION"

#define INTERSTITIAL_REQUESTING @"INTERSTITIAL_REQUEST"
#define INTERSTITIAL_LOADED @"INTERSTITIAL_LOADED"
#define INTERSTITIAL_FAILED @"INTERSTITIAL_FAILED"
#define INTERSTITIAL_SHOW @"INTERSTITIAL_SHOW"
#define INTERSTITIAL_OPEN @"INTERSTITIAL_OPEN"
#define INTERSTITIAL_CLOSE @"INTERSTITIAL_CLOSE"
#define INTERSTITIAL_CLICK @"INTERSTITIAL_CLICK"
#define INTERSTITIAL_IM @"INTERSTITIAL_IMPRESSION"
#define INTERSTITIAL_REMEDY @"INTERSTITIAL_REMEDY"


#define REWARD_REQUESTING @"REWARD_REQUEST"
#define REWARD_LOADED @"REWARD_LOADED"
#define REWARD_FAILED @"REWARD_FAILED"
#define REWARD_PLAY_ERROR @"REWARD_PLAY_ERROR"
#define REWARD_SHOW @"REWARD_SHOW"
#define REWARD_OPEN @"REWARD_OPEN"
#define REWARD_CLOSE @"REWARD_CLOSE"
#define REWARD_COMPLETE @"REWARD_COMPLETE"
#define REWARD_CLICK @"REWARD_CLICK"
#define REWARD_LEFT @"REWARD_LEFT"
#define REWARD_IM @"REWARD_IMPRESSION"
#define REWARD_REMEDY @"REWARD_REMEDY"


#pragma mark   Ad impression Ad

#define AD_IM @"AD_IM"
#define AD_LAST_DATE @"AD_LAST_DATE"
#define AD_DAY_IN @"AD_DAY_IN"
#define AD_DAY_RE @"AD_DAY_RE"
#define AD_IDFA @"AD_IDFA"
#define AD_USER_SOURCE @"AD_USER_SOURCE"
#define AD_ALL_BA @"AD_ALL_BA"
#define AD_ALL_IN @"AD_ALL_IN"
#define AD_ALL_RE @"AD_ALL_RE"


#define AD_FORMAT @"AD_FORMAT"
#define AD_TYPE @"AD_TYPE"
#define AD_ITEM_NAME @"AD_ITEM_NAME"
#define AD_PLACEMENT_NAME @"AD_PLACEMENT_NAME"
#define AD_COUNTRY @"AD_COUNTRY"
#define AD_CURRENCY @"AD_CURRENCY"
#define AD_NETWORK @"AD_NETWORK"
#define AD_NETWORK_ID @"AD_NETWORK_ID"
#define AD_PRECISION @"AD_PRECISION"
#define AD_REVENUE @"AD_REVENUE"


#pragma mark   define
#define IPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size)) : NO)
#define IPhoneXR     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size)) : NO)
#define IPhoneXS     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size)) : NO)

#define IPhoneXRMax     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size)) : NO)

#define IPHONE_4 (IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0)
#define IPHONE_5 (IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IPHONE_6 (IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IPHONE_7 (IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IPHONE_8 (IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)

#define IPHONE_6_PLUS (IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define iPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define iPadPro9 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && [UIScreen mainScreen].bounds.size.height == 1024)
#define iPadPro10 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && [UIScreen mainScreen].bounds.size.height == 1112)
#define iPadPro11 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && [UIScreen mainScreen].bounds.size.height == 1194)
#define iPadPro12 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && [UIScreen mainScreen].bounds.size.height == 1366)
#define IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)



#define iOS7 (([[UIDevice currentDevice].systemVersion intValue] >= 7) ? YES : NO )
#define iOS8 (([[UIDevice currentDevice].systemVersion intValue] >= 8) ? YES : NO )
#define iOS9 (([[UIDevice currentDevice].systemVersion intValue] >= 9) ? YES : NO )
#define iOS10 (([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) ? YES : NO )
#define iOS11 (([[UIDevice currentDevice].systemVersion floatValue] >= 11.0) ? YES : NO )


#define SCREENWIDTH [UIScreen mainScreen].bounds.size.width
#define SCREENHEIGHT [UIScreen mainScreen].bounds.size.height



#define FEEDBACK_DEVICE_INFO_FORMAT @"<br /><br /><br /><br /><font color=\"#9F9F9F\" style=\"font-size: 13px;\"> <i>(%@ %@ on %@ running with iOS %@</i>)</font>"

//#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


#endif /* AdsConstant_h */
