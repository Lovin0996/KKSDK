//
//  Analytics.h
//  KKSDK
//
//  Created by jay Win on 2019/3/28.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface Analytics : NSObject
@property(nonatomic,strong)NSString *trackName;


+(instancetype)shareInstance;

+(void)initAdjust:(BOOL)production;
+(void)initAdjust:(BOOL)production :(NSUInteger)secretId
info1:(NSUInteger)info1
info2:(NSUInteger)info2
info3:(NSUInteger)info3
info4:(NSUInteger)info4;
+(void)initFacebook;
+(void)initFirebase;
+(void)initThinkingData;
    




+(void)logAdjustEvent:(NSString *)eventToken;
+(void)logAdjustRevenue:(NSString *)eventToken price:(double)prices currencyCode:(NSString*)code;


+(void)logFacebookEvent:(NSString *)eventName params:(NSDictionary *)dic;
+(void)logFirebaseEvent:(NSString *)eventName params:(NSDictionary *)dic;
+(void)logFacebookFirebaseEvent:(NSString *)eventName params:(NSDictionary *)dic;
+(void)logFacebookFirebaseThinkEvent:(NSString *)eventName params:(NSDictionary *)dic;



+(void)setTkTrackEvent:(NSString *)trackName params:(NSDictionary *)dic;

+(void)setTKVisitorID:(NSString *)visitorid;
+(void)setTkLoginID:(NSString *)loginID;
+(void)setTkLogout;

+(void)setTkSuperProperties:(NSDictionary *)properties;
+(void)unSetTkSuperProperties:(NSString *)propertyName;
+(void)setTimeTrackEvent:(NSString *)timeName;
+(void)setTkUser:(NSDictionary*)userInfo;
+(void)setTkUserOnce:(NSDictionary*)userInfo;
+(void)setTkUserAdd:(NSDictionary *)addInfo;
+(void)deleteTkUser;

+(void)validateReceiptViaServer:(NSString *)transaction_id purchasePoint:(NSString *)point;

+(NSString *)getIDFA;
+(NSString *)getPushToken;
+(NSString *)getShortVersion;

+(void)registerNotificationWithDiveceToken: (NSData *)diviceToken;
+(void)registerNotificationSetting: (UIApplication *)application;
+(void)registerNotificationWhenAppLaunch:(UIApplication *)application;


@end


