//
//  AdsConfig.m
//  KKSDK
//
//  Created by jay Win on 2019/4/2.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import "AdsConfig.h"


@implementation AdsConfig

static NSString *adjustid;
static NSString *fbid;
static NSString *thinkingDataID;
static NSString *thinkingDataurl;
static NSString *bannerid;
static NSString *padBannerid;
static NSString *interstitialID;
static NSString *otherInterstitialid;
static NSString *rewardVideoID;


static NSString *bannerImpressionToken;
static NSString *interstitialmpressionToken;
static NSString *otherInterstitialmpressionToken;
static NSString *rewardImpressionToken;




+ (void)loadConfig{
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"AdsConfig.plist" ofType:nil];
    if([AdsConfig isBlankString:filePath]){
        NSLog(@"没有找到广告配置表，无法初始化");
        return ;
    }
    NSDictionary *adsIDConfig = [NSDictionary dictionaryWithContentsOfFile:filePath];
    if([adsIDConfig objectForKey:@"Ajust_app_id"]){
        [AdsConfig setAdjustAppID: adsIDConfig[@"Ajust_app_id"]];
    }
    if([adsIDConfig objectForKey:@"Facebook_app_id"]){
        [AdsConfig setFacebookID:adsIDConfig[@"Facebook_app_id"]];
    }
    if([adsIDConfig objectForKey:@"Think_app_id"]){
        [AdsConfig setThinkID:adsIDConfig[@"Think_app_id"]];
    }
    if([adsIDConfig objectForKey:@"Think_app_url"]){
        [AdsConfig setThinkUrl:adsIDConfig[@"Think_app_url"]];
    }
     //根据地区设置不同的banner广告id
    //根据地区设置不同的插屏广告id
    if([[[NSLocale currentLocale]objectForKey:NSLocaleIdentifier]hasSuffix:@"CN"]){
        if([adsIDConfig objectForKey:@"TT_Banner_id"]){
            [AdsConfig setBanner_ID:adsIDConfig[@"TT_Banner_id"]];
        }
    }else{
        if([adsIDConfig objectForKey:@"BannerID"]){
            [AdsConfig setBanner_ID:adsIDConfig[@"BannerID"]];
        }
    }
    
    
    
   
    if([adsIDConfig objectForKey:@"PadBannerID"]){
        [AdsConfig setPad_banner_ID:adsIDConfig[@"PadBannerID"]];
        
    }
  

    if([[[NSLocale currentLocale]objectForKey:NSLocaleIdentifier]hasSuffix:@"CN"]){
        if([adsIDConfig objectForKey:@"TT_Interstitial_id"]){
            [AdsConfig setInterstitial_ID:adsIDConfig[@"TT_Interstitial_id"]];
        }
    }else{
        if([adsIDConfig objectForKey:@"InterstitialID"]){
            [AdsConfig setInterstitial_ID:adsIDConfig[@"InterstitialID"]];
        }
    }
    

    //根据地区设置不同的激励广告id
    if([[[NSLocale currentLocale]objectForKey:NSLocaleIdentifier]hasSuffix:@"CN"]){
        if([adsIDConfig objectForKey:@"TT_RewardVideo_id"]){
            [AdsConfig setRewardVideo_ID:adsIDConfig[@"TT_RewardVideo_id"]];
        }
    }else{
        if([adsIDConfig objectForKey:@"RewardID"]){
            [AdsConfig setRewardVideo_ID:adsIDConfig[@"RewardID"]];
        }
    }
 
    

    if([adsIDConfig objectForKey:@"Adjust_banner_impression"]){
        [AdsConfig setAdjust_banner_impression:adsIDConfig[@"Adjust_banner_impression"]];
    }
    
    
    
    
    if([adsIDConfig objectForKey:@"Adjust_interstitial_impression"]){
        [AdsConfig setAdjust_interstitial_impression:adsIDConfig[@"Adjust_interstitial_impression"]];
    }
    
    
    
    if([adsIDConfig objectForKey:@"Adjust_other_interstitial_impression"]){
        [AdsConfig setAdjust_other_interstitial_impression:adsIDConfig[@"Adjust_other_interstitial_impression"]];
    }
    
    
    if([adsIDConfig objectForKey:@"Adjust_reward_impression"]){
        [AdsConfig setAdjust_reward_impression:adsIDConfig[@"Adjust_reward_impression"]];
    }
    
}

+(BOOL)isBlankString:(NSString *)aStr {
    if (!aStr) {
        return YES;
    }
    if ([aStr isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if (!aStr.length) {
        return YES;
    }
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedStr = [aStr stringByTrimmingCharactersInSet:set];
    if (!trimmedStr.length) {
        return YES;
    }
    return NO;
}


+ (void)setAdjustAppID:(NSString *)adjustAppID{
    adjustid=adjustAppID;
}
+ (NSString *)adjustAppID{
    return  [AdsConfig isBlankString:adjustid]?nil:adjustid;
}
+ (void)setFacebookID:(NSString *)facebookID{
    fbid=facebookID;
}
+ (NSString *)facebookID{
    return  [AdsConfig isBlankString:fbid]?nil:fbid;
}
+ (void)setThinkID:(NSString *)thinkID{
    thinkingDataID=thinkID;
}
+ (NSString *)thinkID{
    return  [AdsConfig isBlankString:thinkingDataID]?nil:thinkingDataID;
}
+ (void)setThinkUrl:(NSString *)thinkUrl{
    thinkingDataurl=thinkUrl;
}
+ (NSString *)thinkUrl{
    return  [AdsConfig isBlankString:thinkingDataurl]?nil:thinkingDataurl;
}
+ (void)setBanner_ID:(NSString *)banner_ID{
    bannerid=banner_ID;
}
+ (void)setPad_banner_ID:(NSString *)pad_banner_ID{
    padBannerid=pad_banner_ID;
}
+ (void)setInterstitial_ID:(NSString *)interstitial_ID{
    interstitialID=interstitial_ID;
}
+ (void)setOther_interstitial_ID:(NSString *)other_interstitial_ID{
    otherInterstitialid=other_interstitial_ID;
}
+ (void)setRewardVideo_ID:(NSString *)rewardVideo_ID{
    rewardVideoID=rewardVideo_ID;
}
+ (NSString *)banner_ID{
    return  [AdsConfig isBlankString:bannerid]?nil:bannerid;
}
+ (NSString *)pad_banner_ID{
    return  [AdsConfig isBlankString:padBannerid]?nil:padBannerid;
}
+ (NSString *)interstitial_ID{
    return  [AdsConfig isBlankString:interstitialID]?nil:interstitialID;
}
+ (NSString *)other_interstitial_ID{
    return  [AdsConfig isBlankString:otherInterstitialid]?nil:otherInterstitialid;
}
+ (NSString *)rewardVideo_ID{
    return  [AdsConfig isBlankString:rewardVideoID]?nil:rewardVideoID;
}

+ (NSString *)adjust_banner_impression{
    return  bannerImpressionToken;
}

+ (void)setAdjust_banner_impression:(NSString *)adjust_banner_impression{
    bannerImpressionToken=adjust_banner_impression;
}

+ (NSString *)adjust_interstitial_impression{
    return  interstitialmpressionToken;
}
+ (void)setAdjust_interstitial_impression:(NSString *)adjust_interstitial_impression{
    interstitialmpressionToken=adjust_interstitial_impression;
}
+ (NSString *)adjust_other_interstitial_impression{
    return  otherInterstitialmpressionToken;
}
+ (void)setAdjust_other_interstitial_impression:(NSString *)adjust_other_interstitial_impression{
    otherInterstitialmpressionToken=adjust_other_interstitial_impression;
}
+ (NSString *)adjust_reward_impression{
    return rewardImpressionToken;
}
+ (void)setAdjust_reward_impression:(NSString *)adjust_reward_impression{
    rewardImpressionToken=adjust_reward_impression;
}
+ (void)refreshAdDate{
    NSString *currentTime=[AdsConfig getCurrentDate];
    if(![[AdsConfig getLastDate] isEqualToString:currentTime]){
        NSLog(@"========日期刷新，重置所有展示");
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:currentTime forKey:AD_LAST_DATE];
        [defaults synchronize];
        [AdsConfig resetAllAdsInDay];
    }
}
+ (void)addBannerImpressionTime{
    NSInteger  times=[AdsConfig getBannerImpressionTimes];
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    times++;
    [defaults setInteger:times forKey:AD_ALL_BA];
    [defaults synchronize];
}
+ (NSInteger )getBannerImpressionTimes{
    return [[NSUserDefaults standardUserDefaults] integerForKey:AD_ALL_BA];
}
+ (void)addInterstitialImpressionTime{
    NSInteger  times=[AdsConfig getInterstitialImpressionTimes];
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    times++;
    [defaults setInteger:times forKey:AD_ALL_IN];
    [defaults synchronize];
}
+ (NSInteger)getInterstitialImpressionTimes{
    return [[NSUserDefaults standardUserDefaults] integerForKey:AD_ALL_IN];
}
+ (void)addInterstitialImpressionTimeInDay{
    NSInteger  times=[AdsConfig getInterstitialImpressionTimesInDay];
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    times++;
    [defaults setInteger:times forKey:AD_DAY_IN];
    [defaults synchronize];
}
+ (NSInteger)getInterstitialImpressionTimesInDay{
    return [[NSUserDefaults standardUserDefaults] integerForKey:AD_DAY_IN];
}
+ (void)addRewardVideoImpressionTimes{
    NSInteger  times=[AdsConfig getRewardVideoImpressionTimes];
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    times++;
    [defaults setInteger:times forKey:AD_ALL_RE];
    [defaults synchronize];
}
+ (NSInteger)getRewardVideoImpressionTimes{
      return [[NSUserDefaults standardUserDefaults] integerForKey:AD_ALL_RE];
}
+ (void)addRewardVideoImpressionTimesInDay{
    NSInteger  times=[AdsConfig getRewardVideoImpressionTimesInDay];
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    times++;
    [defaults setInteger:times forKey:AD_DAY_RE];
    [defaults synchronize];
}
+ (NSInteger)getRewardVideoImpressionTimesInDay{
    return [[NSUserDefaults standardUserDefaults] integerForKey:AD_DAY_RE];
}
//reset times
+(void)resetAllAdsInDay{
    NSUserDefaults *defaults1 = [NSUserDefaults standardUserDefaults];
    [defaults1 setObject:0 forKey:AD_DAY_IN];
    [defaults1 synchronize];
    
    NSUserDefaults *defaults2 = [NSUserDefaults standardUserDefaults];
    [defaults2 setObject:0 forKey:AD_DAY_RE];
    [defaults2 synchronize];
}
+(NSString *)getCurrentDate{
    NSDate *date = [NSDate date];
    NSDateFormatter *forMatter = [[NSDateFormatter alloc] init];
    [forMatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [forMatter stringFromDate:date];
    return dateStr;
}
+(NSString *)getLastDate{
    NSString *lastTime=[[NSUserDefaults standardUserDefaults]objectForKey:AD_LAST_DATE];
    if(lastTime){
        return  lastTime;
    }else{
        return @"";
    }
}
@end
