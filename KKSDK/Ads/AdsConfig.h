//
//  AdsConfig.h
//  KKSDK
//
//  Created by jay Win on 2019/4/2.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdsConstant.h"
#import "CommonUtils.h"

@interface AdsConfig : NSObject

@property(class,nonatomic,strong)NSString *adjustAppID;
@property(class,nonatomic,strong)NSString *facebookID;
@property(class,nonatomic,strong)NSString *thinkID;
@property(class,nonatomic,strong)NSString *thinkUrl;


@property(class,nonatomic,strong)NSString *banner_ID;
@property(class,nonatomic,strong)NSString *pad_banner_ID;
@property(class,nonatomic,strong)NSString *interstitial_ID;
@property(class,nonatomic,strong)NSString *rewardVideo_ID;


@property(class,nonatomic,strong)NSString *adjust_banner_impression;
@property(class,nonatomic,strong)NSString *adjust_interstitial_impression;
@property(class,nonatomic,strong)NSString *adjust_reward_impression;
+(void)loadConfig;

+(void)refreshAdDate;


+(void)addBannerImpressionTime;
+(NSInteger )getBannerImpressionTimes;

+(void)addInterstitialImpressionTimeInDay;
+(NSInteger)getInterstitialImpressionTimesInDay;

+(void)addInterstitialImpressionTime;
+(NSInteger)getInterstitialImpressionTimes;

+(void)addRewardVideoImpressionTimesInDay;
+(NSInteger)getRewardVideoImpressionTimesInDay;

+(void)addRewardVideoImpressionTimes;
+(NSInteger)getRewardVideoImpressionTimes;

@end
