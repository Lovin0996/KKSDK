//
//  TTAdsManager.h
//  Unity-iPhone
//
//  Created by jay Win on 2019/7/15.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AdsConfig.h"
NS_ASSUME_NONNULL_BEGIN
@class TTAdsManager;

@protocol TTAdsManagerDelegate <NSObject>
-(void)onAdsListener:(NSString *)adsState;
@end

@interface TTAdsManager : NSObject

@property (nonatomic, weak) id<TTAdsManagerDelegate>delegate;
+(instancetype)shareInstance;
-(void)initTT:(UIViewController *)rootViewController enableLog:(BOOL)debug;
-(void)requestInterstitial;
-(BOOL)hasInterstitial;
-(void)showInterstitial;
-(void)requestRewardVideo;
-(BOOL)hasRewardVideo;
-(void)setRewardVideoAutoPlayWhenLoadedStatus:(BOOL)enable;
-(void)showRewardVideo;
@end

NS_ASSUME_NONNULL_END
