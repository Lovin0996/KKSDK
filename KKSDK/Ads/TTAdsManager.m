//
//  TTAdsManager.m
//  Unity-iPhone
//
//  Created by jay Win on 2019/7/15.
//

#import "TTAdsManager.h"
#import <BUAdSDK/BUAdSDK.h>
#import <Analytics.h>
#import <AdsConstant.h>

@interface TTAdsManager()<BUFullscreenVideoAdDelegate,BURewardedVideoAdDelegate>
@property BOOL showAdsLog;
@property BOOL isRequestingInterstitial;
@property BOOL isRequestingRewardVideo;
@property BOOL isLoadedInterstitial;
@property BOOL isLoadedRewardVideo;
@property BOOL autoPlayRewardVideo;
@property(nonatomic,strong)BUFullscreenVideoAd *interstitial;
@property (nonatomic, strong) BURewardedVideoAd *rewardedVideoAd;
@property (nonatomic,strong)UIViewController *rootViewController;
@end
@implementation TTAdsManager

+ (instancetype)shareInstance{
    static TTAdsManager *manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [TTAdsManager new];
    });
    return manager;
}
-(void)logAdsInfo:(NSObject *)obj{
    if(_showAdsLog)
        NSLog(@"===========%@",obj);
}

- (void)initTT:(UIViewController *)rootViewController enableLog:(BOOL)debug{
    self.rootViewController=rootViewController;
    self.showAdsLog=debug;
    [self logAdsInfo:@"头条初始化完成"];
    [Analytics setTkTrackEvent:@"INIT_TT_SUCCEED" params:nil];
    [self.delegate onAdsListener:INIT_SUCCEED];
}
#pragma mark   Interstitial
- (void)requestInterstitial{
    if(self.isRequestingInterstitial)
        return;
    [self logAdsInfo:@"请求插屏广告"];
    self.interstitial=nil;
    self.isLoadedInterstitial=NO;
    self.isRequestingInterstitial=YES;
    self.interstitial=[[BUFullscreenVideoAd alloc]initWithSlotID:AdsConfig.interstitial_ID];
    self.interstitial.delegate=self;
    [self.interstitial loadAdData];
    [Analytics setTkTrackEvent:INTERSTITIAL_REQUESTING params:nil];
}

- (BOOL)hasInterstitial{
    if( self.interstitial&&self.isLoadedInterstitial){
        return  YES;
    }else{
        [self requestInterstitial];
        return  NO;
    }
}
- (void)showInterstitial{
    self.isLoadedInterstitial=NO;
    [Analytics setTkTrackEvent:INTERSTITIAL_SHOW params:nil];
    [self.interstitial showAdFromRootViewController:_rootViewController];
}
/**
 This method is called when video ad material loaded successfully. 物料加载成功
 */
- (void)fullscreenVideoMaterialMetaAdDidLoad:(BUFullscreenVideoAd *)fullscreenVideoAd{
    
}
/**
 This method is called when video ad materia failed to load.加载失败
 @param error : the reason of error
 */
- (void)fullscreenVideoAd:(BUFullscreenVideoAd *)fullscreenVideoAd didFailWithError:(NSError *)error{
     NSLog(@"========插屏加载失败%@",[error localizedDescription]);
     self.isRequestingInterstitial=NO;
     [self.delegate onAdsListener:INTERSTITIAL_FAILED];
     [Analytics setTkTrackEvent:INTERSTITIAL_FAILED params:@{@"errorCode":error.localizedDescription}];
}
/**
 This method is called when video ad creatives is cached successfully. 加载成功
 */
- (void)fullscreenVideoAdVideoDataDidLoad:(BUFullscreenVideoAd *)fullscreenVideoAd{
    [self logAdsInfo:@"插屏加载成功"];
    self.isRequestingInterstitial=NO;
    self.isLoadedInterstitial=YES;
    [self.delegate onAdsListener:INTERSTITIAL_LOADED];
    [Analytics setTkTrackEvent:INTERSTITIAL_LOADED params:nil];
}
/**
 This method is called when video ad slot will be showing. 即将打开
 */
- (void)fullscreenVideoAdWillVisible:(BUFullscreenVideoAd *)fullscreenVideoAd{
//      [self logAdsInfo:@"插屏即将打开"];
}
/**
 This method is called when video ad slot has been shown.已经打开
 */
- (void)fullscreenVideoAdDidVisible:(BUFullscreenVideoAd *)fullscreenVideoAd{
    [self logAdsInfo:@"插屏已经打开"];
    _isLoadedInterstitial=NO;
    [self.delegate onAdsListener:INTERSTITIAL_OPEN];
    [Analytics setTkTrackEvent:INTERSTITIAL_OPEN params:nil];
}
/**
 This method is called when video ad is closed.已经关闭
 */
- (void)fullscreenVideoAdDidClose:(BUFullscreenVideoAd *)fullscreenVideoAd{
    [self logAdsInfo:@"插屏已经关闭"];
    [self.delegate onAdsListener:INTERSTITIAL_CLOSE];
    [Analytics setTkTrackEvent:INTERSTITIAL_CLOSE params:nil];
    [Analytics logAdjustEvent:AdsConfig.adjust_interstitial_impression];
    [self requestInterstitial];
}
/**
 This method is called when video ad is clicked.点击
 */
- (void)fullscreenVideoAdDidClick:(BUFullscreenVideoAd *)fullscreenVideoAd{
      [self logAdsInfo:@"插屏点击"];
      [Analytics setTkTrackEvent:INTERSTITIAL_CLICK params:nil];
}

#pragma mark   RewardVideo
- (void)requestRewardVideo{
    if(self.isRequestingRewardVideo)
        return;
    self.rewardedVideoAd=nil;
    [self logAdsInfo:@"请求激励视频"];
    [Analytics setTkTrackEvent:REWARD_REQUESTING params:nil];
    self.isLoadedRewardVideo=NO;
    self.isRequestingRewardVideo=YES;
    BURewardedVideoModel *model = [[BURewardedVideoModel alloc] init];
    model.userId = @"123";
    self.rewardedVideoAd=[[BURewardedVideoAd alloc]initWithSlotID:AdsConfig.rewardVideo_ID rewardedVideoModel:model];
    self.rewardedVideoAd.delegate = self;
    [self.rewardedVideoAd loadAdData];
    
}
- (BOOL)hasRewardVideo{
    if(self.rewardedVideoAd&&self.isLoadedRewardVideo){
        return  YES;
    }else{
        [self requestRewardVideo];
        return NO;
    }
}
- (void)showRewardVideo{
    self.isLoadedRewardVideo=NO;
     [Analytics setTkTrackEvent:REWARD_SHOW params:nil];
     [self.rewardedVideoAd showAdFromRootViewController:self.rootViewController];
}
- (void)setRewardVideoAutoPlayWhenLoadedStatus:(BOOL)enable{
    self.autoPlayRewardVideo=enable;
}
- (void)rewardedVideoAdDidLoad:(BURewardedVideoAd *)rewardedVideoAd{
    [self logAdsInfo:@"激励已经加载成功"];
    self.isRequestingRewardVideo=NO;
    self.isLoadedRewardVideo=YES;
    [self.delegate onAdsListener:REWARD_LOADED];
    [Analytics setTkTrackEvent:REWARD_LOADED params:nil];
    if(self.autoPlayRewardVideo){
        [self showRewardVideo];
    }
}
- (void)rewardedVideoAdDidClick:(BURewardedVideoAd *)rewardedVideoAd{
    [self logAdsInfo:@"激励点击"];
    [Analytics setTkTrackEvent:REWARD_CLICK params:nil];
}

- (void)rewardedVideoAdWillClose:(BURewardedVideoAd *)rewardedVideoAd{

}
- (void)rewardedVideoAdDidClose:(BURewardedVideoAd *)rewardedVideoAd{
     [self logAdsInfo:@"激励已经关闭"];
     self.autoPlayRewardVideo=NO;
     [self.delegate onAdsListener:REWARD_COMPLETE];
     [Analytics setTkTrackEvent:REWARD_COMPLETE params:nil];
     [Analytics logAdjustEvent:AdsConfig.adjust_reward_impression];
     [self requestRewardVideo];
}
- (void)rewardedVideoAdDidVisible:(BURewardedVideoAd *)rewardedVideoAd{
    [self logAdsInfo:@"激励已经打开"];
     _isLoadedRewardVideo=NO;
     self.autoPlayRewardVideo=NO;
    [Analytics setTkTrackEvent:REWARD_OPEN params:nil];
    [self.delegate onAdsListener:REWARD_OPEN];
}
- (void)rewardedVideoAd:(BURewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *)error{
    self.isRequestingRewardVideo=NO;
    self.isLoadedRewardVideo=NO;
    NSLog(@"======激励加载失败%@",error.localizedDescription);
    [self.delegate onAdsListener:REWARD_FAILED];
    [Analytics setTkTrackEvent:REWARD_FAILED params:@{@"errorCode":error.localizedDescription}];
}
- (void)rewardedVideoAdDidPlayFinish:(BURewardedVideoAd *)rewardedVideoAd didFailWithError:(NSError *)error{
    _isLoadedRewardVideo=NO;
    if (error) {
        [self logAdsInfo:@"激励播放错误"];
        [Analytics setTkTrackEvent:REWARD_PLAY_ERROR params:@{@"errorCode":error.localizedDescription}];
    } else {
        [self logAdsInfo:@"激励播放成功"];
    }
}
- (void)rewardedVideoAdServerRewardDidFail:(BURewardedVideoAd *)rewardedVideoAd{
     [self logAdsInfo:@"服务器验证激励失败"];
     [Analytics setTkTrackEvent:@"REWARD_SE_FAILED" params:nil];
}
- (void)rewardedVideoAdServerRewardDidSucceed:(BURewardedVideoAd *)rewardedVideoAd verify:(BOOL)verify{
     [self logAdsInfo:@"服务器验证激励成功"];
     [Analytics setTkTrackEvent:@"REWARD_SE_SUCCEED" params:nil];
}
@end
