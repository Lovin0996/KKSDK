//
//  AdsManager.h
//  KKSDK
//
//  Created by jay Win on 2019/3/8.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AdsConstant.h"



@class AdsManager;

@protocol AdsManagerDelegate <NSObject>
-(void)onAdsListener:(NSString *)adsState;
@end


@interface AdsManager : NSObject



@property (nonatomic, weak) id<AdsManagerDelegate>delegate;
@property(nonatomic,strong)NSString *bannerID;
@property(nonatomic,strong)NSString *padBannerID;
@property(nonatomic,strong)NSString *interstittialID;
@property(nonatomic,strong)NSString *otherInterstittialID;
@property(nonatomic,strong)NSString *rewardID;
@property(nonatomic,strong)NSString *nativeID;

@property (nonatomic,strong)UIViewController *rootViewController;
@property (nonatomic,strong)UIView *bannerContainer;


@property BOOL bannerInTop;
@property BOOL runInUnity;
@property BOOL autoPlayRewardVideo;

+(instancetype)shareInstance;
-(void)initMopub:(UIViewController *)rootViewController enableLog:(BOOL)debug;
-(void)setAdsIntervals:(int)cdTime;
-(void)showBanner:(UIView *)container position:(BOOL)isTop;
-(void)showBanner:(UIView *)container onImpressionEvent:(void(^)(void))onImpression onFailed:(void(^)(void))onFailed;
-(void)hideBanner;

-(void)requestInterstitial;
-(BOOL)hasInterstitial;
-(void)showInterstitial;
-(void)showInterstitial:(void(^)(void))onOpen onCloseEvent:(void(^)(void))onClose;


-(void)requestRewardVideo;
-(BOOL)hasRewardVideo;
-(void)setRewardVideoAutoPlayWhenLoadedStatus:(BOOL)enable;
-(void)showRewardVideo;
-(void)showRewardVideo:(void(^)(void))onOpen onCloseEvent:(void(^)(void))onClose onCompleteEvent:(void(^)(void))onComplete;
@end

