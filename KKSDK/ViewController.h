//
//  ViewController.h
//  KKSDK
//
//  Created by jay Win on 2019/3/7.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIView *BannerView;
@property BOOL isChina;
- (IBAction)showBannerBtn:(id)sender;
- (IBAction)showInterstitialBtn:(id)sender;
- (IBAction)showRewardVideoBtn:(id)sender;

@end

