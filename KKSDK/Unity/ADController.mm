//
//  UnityAdController.m
//  Unity-iPhone
//
//  Created by jay Win on 2019/4/1.
//

#import <Foundation/Foundation.h>
#import <AdSupport/AdSupport.h>
#import "ADController.h"



@implementation ADController


+ (instancetype)instance{
    static ADController *manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [ADController new];
    
        [AdsManager shareInstance].delegate=manager;
        [TTAdsManager shareInstance].delegate=manager;
        [CommonUtils sharedManager].delegate=manager;
    });
    return manager;
}
- (void)onRequestPermissionStateResponse:(NSString *)adsState{
     [self sendPermissionStateMessage:adsState];
}

-(void)sendPermissionStateMessage:(NSString *)permissionState{
#ifdef RUN_WITH_UNITY
    UnitySendMessage("AdsManager", "RequestPermissionCallback", [message UTF8String]);
#endif
//#ifdef RUN_WITH_LAYA
//    [[conchRuntime GetIOSConchRuntime]callbackToJSWithObject:self methodName:@"initMopub:" ret:message];
//#endif
}
- (void)onAdsListener:(NSString *)adsState{
    [self sendMessage:adsState];
}
- (void)sendMessage:(NSString *)message{
#ifdef RUN_WITH_UNITY
    UnitySendMessage("AdsManager", "Callback", [message UTF8String]);
#endif
    
#ifdef RUN_WITH_LAYA
    if(_isChina){
        [[conchRuntime GetIOSConchRuntime]callbackToJSWithObject:self methodName:@"initTT:" ret:message];
    }else{
        [[conchRuntime GetIOSConchRuntime]callbackToJSWithObject:self methodName:@"initMopub:" ret:message];
        
    }
#endif
}


#pragma mark   Laya
- (void)initMopub:(NSNumber *)debug{
    if([[[NSLocale currentLocale]objectForKey:NSLocaleIdentifier]hasSuffix:@"CN"]){
        _isChina=YES;
    }else{
        _isChina=NO;
    }
    [AdsConfig loadConfig];
  
#ifdef RUN_WITH_LAYA
    if(_isChina){
        [TTAdsManager shareInstance].delegate=self;
        [[TTAdsManager shareInstance]initTT:[ViewController GetIOSViewController] enableLog:YES];
    }else{
        [AdsManager shareInstance].delegate=self;
        [[AdsManager shareInstance]initMopub:[ViewController GetIOSViewController]enableLog:debug.intValue==0?NO:YES];
    }
#endif
}
-(void)showBanner:(NSNumber *)isTop{
    
#ifdef RUN_WITH_LAYA
//    [[AdsManager shareInstance]showBanner:ViewController.GetIOSViewController.view position:isTop.intValue==0?NO:YES];
#endif
}
-(void)hideBanner{
    [[AdsManager shareInstance]hideBanner];
}

- (void)requestInterstitial{
    if(_isChina){
         [[TTAdsManager shareInstance]requestInterstitial];
    }else{
         [[AdsManager shareInstance]requestInterstitial];
    }
   
}
- (void)showInterstitial{
    if(_isChina){
        [[TTAdsManager shareInstance]showInterstitial];
    }else{
        [[AdsManager shareInstance]showInterstitial];
    }
 
}
-(void)requestRewardVideo{
    if(_isChina){
         [[TTAdsManager shareInstance]requestRewardVideo];
    }else{
        [[AdsManager shareInstance]requestRewardVideo];
    }
}
-(void)showRewardVideo{
    if(_isChina){
         [[TTAdsManager shareInstance]showRewardVideo];
    }else{
         [[AdsManager shareInstance]showRewardVideo];
    }
    
}
-(void)setRewardVideoAutoPlay:(NSNumber *)enable{
    if(_isChina){
       [[TTAdsManager shareInstance]setRewardVideoAutoPlayWhenLoadedStatus: enable.intValue==0?NO:YES];
    }else{
       [[AdsManager shareInstance]setRewardVideoAutoPlayWhenLoadedStatus: enable.intValue==0?NO:YES];
    }
    
}
-(void)setAdsInterval:(NSNumber *)time{
    if(_isChina){
        
    }else{
         [[AdsManager shareInstance]setAdsIntervals:time.intValue];
    }
}
- (void)getIDFA{
    
#ifdef RUN_WITH_LAYA
    NSString *idfa=[[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    [[conchRuntime GetIOSConchRuntime]callbackToJSWithObject:self methodName:@"getIDFA" ret:idfa];
#endif
}
@end





#pragma mark   Unity
extern "C"{
    void initMopub(bool debug){
        if([[[NSLocale currentLocale]objectForKey:NSLocaleIdentifier]hasSuffix:@"CN"]){
           [ADController instance].isChina=YES;
        }else{
           [ADController instance].isChina=NO;
        }
#ifdef RUN_WITH_UNITY
        if([ADController instance].isChina){
              [TTAdsManager shareInstance]initTT:UnityGetGLViewController() enableLog:debug];
        }else{
            [[AdsManager shareInstance]initMopub:UnityGetGLViewController() enableLog:debug];
            [AdsManager shareInstance].bannerContainer=UnityGetGLView();
        }
        
#endif
    }
    void setAdsInterval(int time){
     
        [[AdsManager shareInstance ]setAdsIntervals:time];
    }
    void showBanner(bool top){
        [AdsManager shareInstance].runInUnity=YES;
#ifdef RUN_WITH_UNITY
        [[AdsManager shareInstance]showBanner:UnityGetGLView() position:top];
#endif
    }
    void hideBanner(){
        [AdsManager shareInstance].runInUnity=YES;
        [[AdsManager shareInstance]hideBanner];
    }
    void requestInterstitial(){
        
        if( [ADController instance].isChina){
            [[TTAdsManager shareInstance]showInterstitial];
        }else{
            [AdsManager shareInstance].runInUnity=YES;
            [[AdsManager shareInstance]showInterstitial];
        }

    }
    bool hasInterstitial(){
        if([ADController instance].isChina){
            return [[TTAdsManager shareInstance]hasInterstitial];
        }else{
            return [[AdsManager shareInstance]hasInterstitial];
        }

    }
    void showInterstitial(){
        if([ADController instance].isChina){
             [[TTAdsManager shareInstance]showInterstitial];
        }else{
            [AdsManager shareInstance].runInUnity=YES;
            [[AdsManager shareInstance]showInterstitial];
        }

    }
    void requestRewardVideo(){
        if( [ADController instance].isChina){
              [[TTAdsManager shareInstance]requestRewardVideo];
        }else{
            [AdsManager shareInstance].runInUnity=YES;
            [[AdsManager shareInstance]requestRewardVideo];
        }

    }
    bool hasRewardVideo(){
        if( [ADController instance].isChina){
                return  [[TTAdsManager shareInstance]hasRewardVideo];
        }else{
                    return  [[AdsManager shareInstance]hasRewardVideo];
        }

    }
    void showRewardVideo(){
        if([ADController instance].isChina){
            [[TTAdsManager shareInstance]showRewardVideo];
        }else{
            [AdsManager shareInstance].runInUnity=YES;
            [[AdsManager shareInstance]showRewardVideo];
        }

    }
    void setRewardVideoAutoPlayWhenLoadedStatus(bool enable){
        if([ADController instance].isChina){
             [[TTAdsManager shareInstance]setRewardVideoAutoPlayWhenLoadedStatus:enable];
        }else{
             [[AdsManager shareInstance]setRewardVideoAutoPlayWhenLoadedStatus:enable];
        }
    }
}
