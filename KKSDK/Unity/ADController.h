//
//  ADController.h
//  KKSDK
//
//  Created by jay Win on 2019/4/1.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AdsManager.h>
#import <AdSupport/AdSupport.h>
#import <CommonUtils.h>
#import <TTAdsManager.h>
@interface ADController : NSObject<AdsManagerDelegate,TTAdsManagerDelegate,CommonUtilsListener>
@property BOOL isChina;
+(instancetype)instance;
@end

