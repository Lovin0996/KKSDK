//
//  MPStaticNativeAdView.m
//  MoPubSampleApp
//
//  Copyright (c) 2015 MoPub. All rights reserved.
//

#import "MPStaticNativeAdView.h"
#import "MPNativeAdRenderingImageLoader.h"
#import <AdsConstant.h>

@implementation MPStaticNativeAdView

- (id)initWithFrame:(CGRect)frame
{
    float w=UIScreen.mainScreen.bounds.size.width;
    float h=UIScreen.mainScreen.bounds.size.height;
    float scale=w/320;

    NSLog(@"=========宽度%f",scale);
    NSLog(@"=========高度%f",h);
    
    self = [super initWithFrame:frame];
    
    
    
    
    
    if (self) {
        CGFloat bannerH = 50;
        CGFloat imageW = 40;

        CGFloat  ctaLabelW = 70;
        CGFloat ctaLabelH = 22;
        if (iPad) {
            bannerH = 90;
            imageW = 65;
            ctaLabelW = 140;
            ctaLabelH = 35;
        }
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12*scale, (bannerH - imageW)/2, imageW, imageW)];
        [self.iconImageView setBackgroundColor:UIColor.redColor];
        [self addSubview:self.iconImageView];
        
        
        
        
        
//        self.mainImageView = [[UIImageView alloc] initWithFrame:CGRectMake(14*scale, (bannerH - imageW)/2, 38, 38)];
//        [self.mainImageView setBackgroundColor:UIColor.blueColor];
//        [self addSubview:self.mainImageView];
        
        
        
       

        self.ctaLabel = [[UILabel alloc] initWithFrame:CGRectMake(w - ctaLabelW - 18, (bannerH - 22)/2, ctaLabelW, ctaLabelH)];
        if (iPad) {
            [self.ctaLabel setFont:[UIFont systemFontOfSize:18]];
        }else{
            [self.ctaLabel setFont:[UIFont systemFontOfSize:13]];
        }
        [self.ctaLabel setText:@"CTA Text"];
        self.ctaLabel.layer.cornerRadius=10.0f;
        [self.ctaLabel setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0f]];
        [[self.ctaLabel layer] setMasksToBounds:YES];
        [self.ctaLabel setTextColor:[UIColor colorWithRed:67/255.0 green:144/255.0 blue:255/255.0 alpha:1.0f]];
        [self.ctaLabel setTextAlignment:NSTextAlignmentCenter];

        [self addSubview:self.ctaLabel];

        //content text
        CGFloat mainTextLabelY = iPad?45:25;
        self.mainTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.iconImageView.frame)+12,mainTextLabelY, CGRectGetMinX(self.ctaLabel.frame) - CGRectGetMaxX(self.iconImageView.frame) -  24, bannerH - mainTextLabelY-5 )];

        if (iPad) {
            [self.mainTextLabel setFont:[UIFont systemFontOfSize:15]];
        }else{
            [self.mainTextLabel setFont:[UIFont systemFontOfSize:11]];
        }
        self.mainTextLabel.minimumScaleFactor = 9;
        [self.mainTextLabel setText:@"Text"];
        [self.mainTextLabel setNumberOfLines:0];
        [self.mainTextLabel setMinimumScaleFactor:0.2f];
        [self addSubview:self.mainTextLabel];


        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.mainTextLabel.frame),iPad?15:7, CGRectGetMinX(self.ctaLabel.frame) - CGRectGetMaxX(self.iconImageView.frame) -  24, iPad?24:14)];
        if (iPad) {
            [self.titleLabel setFont:[UIFont systemFontOfSize:18]];
        }else{
            [self.titleLabel setFont:[UIFont systemFontOfSize:15]];
        }
        [self.titleLabel setNumberOfLines:0];
        [self.titleLabel setMinimumScaleFactor:0.2f];
        [self.titleLabel setText:@"Title"];
        [self addSubview:self.titleLabel];


        self.backgroundColor = [UIColor colorWithWhite:1.0f alpha:1.0f];
        [self.titleLabel setTextColor:[UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0f]];
        [self.mainTextLabel setTextColor:[UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0f]];
    }
    return self;
}

- (void)layoutSubviews
{

}

#pragma mark - <MPNativeAdRendering>

- (UILabel *)nativeMainTextLabel
{
    return self.mainTextLabel;
}

- (UILabel *)nativeTitleTextLabel
{
    return self.titleLabel;
}

- (UILabel *)nativeCallToActionTextLabel
{
    return self.ctaLabel;
}

- (UIImageView *)nativeIconImageView
{
    return self.iconImageView;
}

- (UIImageView *)nativeMainImageView
{
    return self.mainImageView;
}

- (UIImageView *)nativePrivacyInformationIconImageView
{
    return self.privacyInformationIconImageView;
}



-(UIImage *)compressOriginalImage:(UIImage *)image toSize:(CGSize)size{
    UIImage * resultImage = image;
    UIGraphicsBeginImageContext(size);
    [resultImage drawInRect:CGRectMake(00, 0, size.width, size.height)];
    UIGraphicsEndImageContext();
    return image;
}



- (void)layoutCustomAssetsWithProperties:(NSDictionary *)customProperties imageLoader:(MPNativeAdRenderingImageLoader *)imageLoader{
    if(customProperties[@"source"]){
        self.source=customProperties[@"source"];
    }
    float w=UIScreen.mainScreen.bounds.size.width;
    float h=UIScreen.mainScreen.bounds.size.height;
//    float scale=w/320;

    CGFloat bannerH = 50;
    CGFloat imageW = 40;

    CGFloat  ctaLabelW = 70;
    CGFloat ctaLabelH = 22;
    CGFloat adTextH = 10;

    if([self.source isEqualToString:@"TT"]){
        self.adText = [[UILabel alloc] initWithFrame:CGRectMake(w-30, bannerH-12, 20, 10)];
        [self addSubview:self.adText];
        [self.adText setText:@"广告"];
        [self.adText setFont:[UIFont systemFontOfSize:8.0f]];

        [self.adText setTextColor:[UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0f]];

        [self.adText setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:self.adText];
        
        

    }else if([self.source isEqualToString:@"FB"]){
//        self.adText = [[UILabel alloc] initWithFrame:CGRectMake(w-45, bannerH-12, 35, 10)];
//        [self addSubview:self.adText];
//        [self.adText setText:@"AdChoices"];
//        [self.adText setFont:[UIFont systemFontOfSize:6.0f]];
//
//        [self.adText setTextColor:[UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0f]];
//
//        [self.adText setTextAlignment:NSTextAlignmentCenter];
//        [self addSubview:self.adText];
//
//        self.adLogo = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.adText.frame) - 15, bannerH-12, 10, 10)];
//        FBAdImage *fbAdchoiceImage=customProperties[@"adChoicesIcon"];
//        __weak typeof(self) weakSelf = self;
//        [fbAdchoiceImage loadImageAsyncWithBlock:^(UIImage * _Nullable image) {
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//                weakSelf.adLogo.image=image;
//                [weakSelf addSubview:weakSelf.adLogo];
//            });
//
//
//        }];
    }else if([self.source isEqualToString:@"ADMOB"]){
            // NSLog(@"==展示ADMOB的广告");
//            self.adText = [[UILabel alloc] initWithFrame:CGRectMake(w-45, bannerH-12, 35, 10)];
//            [self addSubview:self.adText];
//            [self.adText setText:@"AD"];
//            [self.adText setFont:[UIFont systemFontOfSize:6.0f]];
//
//            [self.adText setTextColor:[UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0f]];
//
//            [self.adText setTextAlignment:NSTextAlignmentCenter];
//            [self addSubview:self.adText];
        }

}

@end
