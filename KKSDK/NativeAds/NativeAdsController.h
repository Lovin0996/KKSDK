//
//  NativeAdsController.h
//  KKSDK
//
//  Created by jay Win on 2019/4/1.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MPNativeAdRequest.h"
#import "MPNativeAd.h"
#import "MPNativeAdRequestTargeting.h"
#import "MPStaticNativeAdView.h"
#import "MPNativeAdDelegate.h"
#import "MPStaticNativeAdRenderer.h"
#import "MPStaticNativeAdRendererSettings.h"
#import "MOPUBNativeVideoAdRendererSettings.h"
#import "MOPUBNativeVideoAdRenderer.h"
#import "MPNativeAdConstants.h"


@interface NativeAdsController : NSObject<MPNativeAdDelegate>
@property (nonatomic, strong) UIView *viewContainner;
@property (nonatomic, strong) UIViewController *rootVC;
@property (nonatomic, strong) MPNativeAdRequest *adRequest;
@property (nonatomic, strong) MPNativeAd *nativeAd;

-(void)request:(UIViewController *)rootViewController container:(UIView *)viewController;

@end

