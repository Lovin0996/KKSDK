//
//  IAPManager.h
//  KKSDK
//
//  Created by jay Win on 2019/5/15.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <Foundation/Foundation.h>

#define RECEIPT_VALIDATION_URL_FORMAT @"http://config.picfuntech.com/services/iap/validating/receipts?productId=%@&isProduction=%@"

NS_ASSUME_NONNULL_BEGIN
@interface IAPManager : NSObject


+ (instancetype)shareManager;
-(void)purchaseItem:(NSString *)productionID;
- (BOOL)hasSubscribed;
- (void)validateReceiptViaServer;
- (void)refreshSubscriptionState;
-(BOOL)hasRemoveAds;
- (void)restore;

-(void)queryProductInfo;

@property (nonatomic , assign) BOOL isPurchase;

//@property (nonatomic , assign)  CGFloat oneTimePrice;
@property (nonatomic , strong)  NSString * currencySymbol;
@end

NS_ASSUME_NONNULL_END
