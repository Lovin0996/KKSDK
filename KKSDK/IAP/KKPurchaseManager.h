//
//  KKPurchaseManager.h
//  KKSDK
//
//  Created by jay Win on 2019/8/16.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@protocol KKPurchaseDelegate <NSObject>
-(void)onComplete:(NSString *)resultInfo;
@end



@interface KKPurchaseManager : NSObject
@property (nonatomic, weak) id<KKPurchaseDelegate>delegate;

+(instancetype)shareManager;

-(void)initPurchase:(BOOL)showLog allInfo:(NSSet *)productInfos completion:(void(^)(NSString *resultIno))onComplete;

-(void)purchaseProduct:(NSString *)productID;
-(BOOL)isSubcribeUser;
-(BOOL)isRemoveAdsUser;
-(void)restorePurchase;
@end

NS_ASSUME_NONNULL_END
