//
//  AppDelegate.h
//  KKSDK
//
//  Created by jay Win on 2019/3/7.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

