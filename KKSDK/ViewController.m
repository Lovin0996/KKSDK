//
//  ViewController.m
//  KKSDK
//
//  Created by jay Win on 2019/3/7.
//  Copyright © 2019 jay Win. All rights reserved.
//

#import "ViewController.h"
#import "Ads/AdsManager.h"
#import "NativeAds/NativeAdsController.h"
#import "Ads/AdsConfig.h"
#import <Analytics.h>
#import "IAP/KKPurchaseManager.h"
#import <CommonUtils.h>
#import "Ads/TTAdsManager.h"



@interface ViewController ()<AdsManagerDelegate,TTAdsManagerDelegate>

@end

@implementation ViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [AdsManager shareInstance].delegate=self;
    [TTAdsManager shareInstance].delegate=self;
    
    
    NSLog(@"========%@",[[NSLocale currentLocale]objectForKey:NSLocaleIdentifier]);
    if([[[NSLocale currentLocale]objectForKey:NSLocaleIdentifier]hasSuffix:@"CN"]){
        _isChina=YES;
        [[TTAdsManager shareInstance]initTT:self enableLog:YES];
        NSLog(@"TT");
    }else{
          _isChina=NO;
        [[AdsManager shareInstance]initMopub:self enableLog:YES];
        NSLog(@"mopub");
    }
    


}
- (void)onAdsListener:(NSString *)adsState{
    if([adsState isEqualToString:INIT_SUCCEED]){
        if(_isChina){
            [[TTAdsManager shareInstance]requestInterstitial];
            [[TTAdsManager shareInstance]requestRewardVideo];
         
        }else{
            [[AdsManager shareInstance]requestInterstitial];
            [[AdsManager shareInstance]requestRewardVideo];
        }
    }
    if([adsState isEqualToString:INTERSTITIAL_LOADED]){
    }
    if([adsState isEqualToString:INTERSTITIAL_FAILED]){
    }
    if([adsState isEqualToString:REWARD_LOADED]){
    }
    if([adsState isEqualToString:REWARD_FAILED]){
    }
}
- (IBAction)showBannerBtn:(id)sender {
    [[AdsManager shareInstance]showBanner:self.BannerView onImpressionEvent:^{
         NSLog(@"=====Banner显示成功");
    } onFailed:^{
         NSLog(@"=====Banner加载失败");
    }];
}
- (IBAction)hiderBanner:(id)sender {
    [[AdsManager shareInstance]hideBanner];
}
- (IBAction)showInterstitialBtn:(id)sender {
    if(!_isChina){
        if(AdsManager.shareInstance.hasInterstitial){
            [[AdsManager shareInstance]showInterstitial:^{
                NSLog(@"======插屏广告打开");
            } onCloseEvent:^{
                NSLog(@"======插屏广告关闭");
            }];
        }else{
            NSLog(@"============插屏尚未准备好，正在请求中");
        }
    }else{
        if([[TTAdsManager shareInstance]hasInterstitial]){
            [[TTAdsManager shareInstance]showInterstitial];
        }else{
             NSLog(@"============插屏尚未准备好，正在请求中");
        }
    }
}
- (IBAction)showRewardVideoBtn:(id)sender {
    if(!_isChina){
        if([[AdsManager shareInstance]hasRewardVideo]){
            [[AdsManager shareInstance]showRewardVideo:^{
                NSLog(@"=====激励广告打开");
            } onCloseEvent:^{
                //todo amdob 广告有中途关闭的特性
                NSLog(@"=====激励广告关闭");
            } onCompleteEvent:^{
                NSLog(@"=====激励广告完成");
            }];
        }
    }else{
        if([[TTAdsManager shareInstance]hasRewardVideo]){
             [[TTAdsManager shareInstance]showRewardVideo];
        }else{
             NSLog(@"=====激励广告没有加载完成");
        }
    }
}
- (IBAction)buy:(id)sender {
    NSLog(@"========buy");
    
}
@end
